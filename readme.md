| **Spike ID**        | B1                                                                                     | **Time Needed** | 1 Hour |
|---------------------|----------------------------------------------------------------------------------------|-----------------|--------|
| **Title**           | Base Spike 1 – Setting up your Development Enviroment                                  |
| **Personnel**       | Mathew Ponting (blueprogrammer)                                                        |
| **Repository Link** | <https://bitbucket.org/blueprogrammer/spike-1-setting-up-your-development-environment> |

Introduction
============

The user will create a git repository on a website of their choosing
(bitbucket for this example). The user will also begin to understand the
requirements for setting up a repository for projects created in Unreal
Engine 4 (C++).

TRT (Technology, Resources and Tools)
=====================================

Usually a reference of tools that are needed to be used for the
completion of this spike.

| **Resource**                                                                                         | **Needed/Recommended** |
|------------------------------------------------------------------------------------------------------|------------------------|
| Bitbucket.org (Or website of choosing to hold a git repository).                                     | Needed                 |
| SourceTree (To clone and create your repository)                                                     | Needed                 |
| Unreal Engine 4 (Not needed if project already exists).                                              | Needed                 |
| Unreal Engine Ignore File - <https://github.com/github/gitignore/blob/master/UnrealEngine.gitignore> | Needed                 |

Goals
=====

1.  Create a new Git Repository on bitbucket and clone it onto your
    local directory (Must be a new folder and must have nothing in it).

2.  Create a .gitignore file via Source Tree that is for
    Unreal Projects. (If you can make a file with no name and just named
    .gitignore, skip to step c).

    1.  Create a new file of choosing in the repository’s directory.
        This is going to be the file you will initially ignore when
        creating your gitignore in sourcetree.

    2.  Using SourceTree, ignore the file which in turn will create the
        new gitignore.

    3.  Using the Unreal Engine Ignore File Link, Copy and paste it into
        the .gitignore file using a text editor (recommended to
        use Notepad++). Make a Commit on your repository after this as
        it will be the base of your project which will start ignoring
        files for an unreal project.

3.  Place the Unreal Project into the Repository’s Directory, Commit
    File and Push.

    1.  (If not already) Create a new Unreal Project (Separate from the
        Repository’s Directory).

        (BEFORE PROCEEDING) Make sure the unreal project you are moving
        is closed. If not, it may cause problems).

    2.  Move (or copy) the contents of the project’s folder (Where the
        .uproject is) into the repository (After moving, if both the
        .gitignore and the .uproject file are in the same place, you
        have done it correctly).

4.  Create a ReadMe.md in the directory. Right now, place a small or
    detailing information about this project (I usually put this report
    in as md form). This will also appear at this repository’s
    home page.

5.  Make a commit and push the project. This will upload it into your
    Repository on bitbucket.

Discovered
==========

-   Using a .gitignore file will allow the temporary files and
    non-needed files to upload.

-   You can make repositories public/private depending on who you want
    to see them.

-   You cannot make files with no name on windows which can be very
    annoying when creating .gitignore files.

Open Issues and Risks
=====================

Anything that was unachievable in this spike goes here. Risks also go in
here.

Issues
------

As mentioned above, you cannot make files on a windows PC that have no
names and just a file extension. This is because Windows has locked
access to doing this to prevent errors and cannot be undone except by
modifying the operating systems. You can avoid this by using cmd
(Command Prompt) but it is much easier to do it through SourceTree.

Risks
-----

No Risks in this spike.

Recommendations
===============

-   Though not covered in the tasks, using a git flow method will allow
    a more organized way of working on a project. A git flow can be
    created in SourceTree. To understand more about git flow, follow
    this link:
    <https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow>

-   Able to use a .gitignore effectively will allow control over a
    project and can be updated when need be.

-   Using SourceTree (Commiting and Pushing), you will be able to see
    your project’s history and also revert back to make hot fixes in
    case massive bugs pop up.


