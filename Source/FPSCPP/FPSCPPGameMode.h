// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "FPSCPPGameMode.generated.h"

UCLASS(minimalapi)
class AFPSCPPGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AFPSCPPGameMode();
};



