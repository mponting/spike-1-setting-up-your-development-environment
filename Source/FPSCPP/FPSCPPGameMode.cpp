// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSCPP.h"
#include "FPSCPPGameMode.h"
#include "FPSCPPHUD.h"
#include "FPSCPPCharacter.h"

AFPSCPPGameMode::AFPSCPPGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSCPPHUD::StaticClass();
}
